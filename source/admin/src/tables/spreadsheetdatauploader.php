<?php

// load help classes
use Joomla\CMS\Table\Table as JTable;

/**
 * @version		$Id: k2item.php 303 2010-01-07 02:56:33Z joomlaworks $
 * @package		K2
 * @author    JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

class Tablespreadsheetdatauploader extends JTable {

	var $id = null;
	var $name = null;
	var $params = null;
	var $active = null;

	function __construct(&$db) {

		parent::__construct('#__spreadsheetdatauploader', 'id', $db);
	}

	function check() {

		if (trim($this->name) == '') {
			$this->setError(JText::_('Item must have a name'));
			return false;
		}

		return true;
	}

	function bind($array, $ignore = '') {

		if (key_exists('params', $array) && is_array($array['params'])) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
		}

		return parent::bind($array, $ignore);
	}
}
