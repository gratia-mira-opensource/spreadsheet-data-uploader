<?php

namespace ChristophBerger\Component\SpreadsheetDataUploader\Administrator\Controller;

defined('_JEXEC') or die;

// Hilfsklassen laden
require_once(JPATH_COMPONENT_ADMINISTRATOR . '/src/Helper/BasicHelper.php');
require_once(JPATH_COMPONENT_ADMINISTRATOR . '/src/Helper/Tasks.php');


use Joomla\CMS\MVC\Controller\BaseController;
use Joomla\CMS\Factory as JFactory;

use ChristophBerger\Component\SpreadsheetDataUploader\Administrator\Helper\TasksController as TasksController;

// do Tasks?
$get = JFactory::getApplication();
$input = $get->input;
$task = $input->get('task','', 'String');
if (!empty($task)) {
    new TasksController($task);
} else {
    /**
     * @package     Joomla.Administrator
     * @subpackage  com_bibelverse
     *
     * @copyright   Copyright (C) 2020 Christoph J. Berger.admin/src All rights reserved.
     * @license     GNU General Public License version 3; see LICENSE
     */

    class DisplayController extends BaseController
    {

    /**
     * Default Controller of bibelverse component
     *
     * @package     Joomla.Administrator
     * @subpackage  com_bibelverse
     */

        /**
         * The default view for the display method.
         *
         * @var string
         */

        protected $default_view = 'Uploader';

        public function display($cachable = false, $urlparams = array())
        {
            return parent::display($cachable, $urlparams);
        }
    }
}
?>