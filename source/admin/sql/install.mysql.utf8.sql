--
-- install
--
CREATE TABLE IF NOT EXISTS `#__spreadsheetdatauploader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `active` int(1) NOT NULL,
  `component` varchar(10) NOT NULL,

  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
--
-- create data
--
INSERT INTO `#__spreadsheetdatauploader` (`id`, `name`, `params`, `active`, `component`) VALUES
(1, 'Einfache Artikel', 'state=0\ncatid=-1\nfeatured=0\nremovetags=0\ncatstate=1\ncreated_by_alias=\naccess=1\naccess_column=\ncreated=\npublish_up=\npublish_down=\nshow_title=\nlink_titles=\nshow_tags=\nshow_intro=\ninfo_block_position=\nshow_category=\nlink_category=\nshow_parent_category=\nlink_parent_category=\nshow_author=\nlink_author=\nshow_create_date=\nshow_modify_date=\nshow_publish_date=\nshow_item_navigation=\nshow_icons=\nshow_print_icon=\nshow_email_icon=\nshow_vote=\nshow_hits=\nshow_noauth=\nurls_position=\nalternative_readmore=\narticle_layout=\nlanguage=*\ndescription={D},\nkeywords={A},\nxreference=\nrobots_select=index, follow\nrobots=\nauthor=\nrights=\nconfigname=Einfache Artikel\nconfigid=1\noption=com_focontentuploader\ncontroller=focontentuploader\ntask=savesettings\ncomponent=content\nfirstdatarow=2\nmax_articles=200\ntitle=A\nalias=\ncatcol=B\nparentcatcol=\nintrotext={C}', 1, 'content');