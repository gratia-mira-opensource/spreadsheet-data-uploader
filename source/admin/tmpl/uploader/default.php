<?php

/**
 * Copyright (C) 2021 Christoph J. Berger (www.gratia-mira.ch)
 * This is an adapted form of freakedout (www.freakedout.de)
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 * */

// no direct access
defined('_JEXEC') or die('Restricted access');?>

<div class="cuContainer">
    <form action="index.php?option=com_spreadsheetdatauploader" enctype="multipart/form-data" method="post" name="adminForm" id="adminForm">
        <div id="getstarted">
            <div id="getstartedtxt">
                <?php echo JText::_('CU_CONTENT_UPLOADER_FREE');?>!</span></h3>
                <p><?php echo JText::_('CU_SELECT_THE_CATEGORY');?><a id="paramslink" class="red pointer" onclick="window.scroll(0,270)"> <?php echo JText::_('CU_CONFIGURATION');?></a>.</p>
            </div>
        </div>
        <div id="uploadFileContainer" class="whiteBox">
            <div class="clearfix">
                <label for="file" class="inline left" style="padding: 7px 0 0 0;font-weight: bold;">
                    <?php echo JText::_('CU_UPLOAD_FILE');?>:
                </label>
                <div class="fileupload fileupload-new inline left" data-provides="fileupload">
                    <div class="input-append">
                        <div class="uneditable-input span3">
                            <i class="icon-file fileupload-exists"></i>
                            <span class="fileupload-preview"></span>
                        </div>
                        <span class="btn btn-file">
                            <span class="fileupload-new"><?php echo JText::_('CU_SELECT_FILE');?></span>
                            <span class="fileupload-exists"><?php echo JText::_('CU_CHANGE');?></span>
                            <input type="file" name="file" id="uploadFile" class="hasTip" title="<?php echo JText::_('CU_TIPUPLOADFILE');?>" >
                        </span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><?php echo JText::_('CU_REMOVE');?></a>
                    </div>
                </div>
                <input type="submit" value="<?php echo JText::_('CU_START_UPLOAD');?>" class="btn btn-primary inline left" onclick="Joomla.submitbutton('startupload')" />
            </div>
        </div>
        <input type="hidden" name="option" value="com_spreadsheetdatauploader" >
        <input type="hidden" name="task" value="startupload" >
        <input type="hidden" name="component" value="content" >
        <input type="hidden" name="configdelete" value="<?php echo $this->config['configid']; ?>" >
    </form>

    <div style="margin: 1em 10px;"><?php echo JText::_('CU_CHOOSECONFIG'); ?></div>

    <form action="index.php?option=com_spreadsheetdatauploader" method="post" name="settingsForm" enctype="multipart/form-data">
        <table width="100%">
            <tr>
                <td valign="top">
                    <div class="whiteBox">
                        <table class="paramsTable" width="100%">
                            <tr>
                                <td>
                                    <input type="submit" value="<?php echo JText::_('CU_SAVE_CONFIGURATION', true); ?>"  class="btn btn-primary inline left" >
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="firstdatarow" title="<?php echo JText::_('CU_TIPFIRSTDATAROW'); ?>" class="hasTip"><?php echo JText::_('CU_FIRST_DATA_ROW'); ?></label>
                                </td>
                                <td>
                                    <input class="columnRef" type="text" name="config[firstdatarow]" id="firstdatarow" size="4" maxlength="5" value="<?php echo $this->config['firstdatarow']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap">
                                    <label for="max_articles" title="<?php echo JText::_('CU_TIPMAXARTICLES'); ?>" class="hasTip"><?php echo JText::_('CU_MAX_ARTICLES'); ?></label>
                                </td>
                                <td colspan="3">
                                    <input class="columnRef" type="text" name="config[max_articles]" id="max_articles" size="4" maxlength="5" defalut="200" />
                                    <span style="position: relative;top: -4px;left: 10px;">
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="title" title="<?php echo JText::_('CU_TIPTITLECOL'); ?>" class="hasTip"><?php echo JText::_('JGLOBAL_TITLE'); ?></label>
                                </td>
                                <td>
                                    <input class="columnRef" type="text" name="config[title]" id="title" size="4" maxlength="2" value="<?php echo $this->config['title']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="alias" title="<?php echo JText::_('CU_TIPALIAS'); ?>" class="hasTip"><?php echo JText::_('JFIELD_ALIAS_LABEL'); ?></label>
                                </td>
                                <td>
                                    <input class="columnRef" type="text" name="config[alias]"	id="alias" size="4" maxlength="2" value="<?php echo $this->config['alias']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="state_col" title="<?php echo JText::_('CU_TIPSTATECOL'); ?>" class="hasTip"><?php echo JText::_('JPUBLISHED'); ?></label>
                                </td>
                                <td>
                                    <div class="radio btn-group inline left">
                                        <?php echo $this->lists['state']; ?>
                                    </div>
                                    <span style="position:relative;top: 4px;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="catid" title="<?php echo JText::_('CU_TIPCATID'); ?>" class="hasTip"><?php echo JText::_('JCATEGORY'); ?></label>
                                </td>
                                <td colspan="3">
                                    <select name="catid" id="catid" style="width: 88%">
                                        <option value="-1"><?php echo JText::_('JOPTION_SELECT_CATEGORY'); ?></option>
                                        <?php echo JHtml::_('select.options', JHtml::_('category.options', 'com_content'), 'value', 'text', $this->config['catid']); ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="featured" title="<?php echo JText::_('CU_TIPFEATUREDCOL'); ?>" class="hasTip"><?php echo JText::_('JFEATURED'); ?></label>
                                </td>
                                <td>
                                    <div class="radio btn-group inline left">
                                        <?php echo $this->lists['featured'];?>
                                    </div>
                                    <span style="position:relative;top: 4px;">
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="catcol" title="<?php echo JText::_('CU_TIPCATCOL'); ?>" class="hasTip"><?php echo JText::_('CU_CATEGORY_COLUMN'); ?></label>
                                </td>
                                <td>
                                    <input type="text" name="config[catcol]" id="catcol" class="columnRef" size="4" maxlength="2" value="<?php echo $this->config['catcol']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="catcolRef" title="<?php echo JText::_('CU_TIPPARENTCATCOLK2'); ?>" class="hasTip"><?php echo JText::_('CU_PARENT_CATEGORY_COLUMN'); ?></label>
                                </td>
                                <td>
                                    <input type="text" name="config[parentcatcol]" id="catcolRef" class="columnRef" size="4" maxlength="2" value="<?php echo @$this->config['parentcatcol']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="removetags" title="<?php echo JText::_('CU_TIPREMOVETAGS'); ?>" class="hasTip"><?php echo JText::_('CU_REMOVE_HTML'); ?></label>
                                </td>
                                <td>
                                    <div class="radio btn-group inline left">
                                        <?php echo $this->lists['removetags']; ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="catstate" title="<?php echo JText::_('CU_TIPCATSTATE'); ?>" class="hasTip"><?php echo JText::_('CU_CATSTATE'); ?></label>
                                </td>
                                <td>
                                    <div class="radio btn-group inline left">
                                        <?php echo $this->lists['catstate']; ?>
                                    </div>
                                </td>
                            </tr>
                            <tr></tr>
                            <tr style="height:auto">
                                <td colspan="2"><?php echo JText::_('CU_IF_CATEGORYDD'); ?></td>
                            </tr>
                        </table>
                    </div>
                    <div id="content-data-tabs" class="whiteBox" style="margin-top: 3px;">
                        <?php echo JText::_('CU_CONTENT_ARTICLE_CONTENT', true); ?><br>
                            <?php echo JText::_('CU_EDITORINFO');?>
                            <br>
                            <br>
                            <label for="config[introtext]" title="<?php echo JText::_('CU_INTROTEXT'); ?>" class="hasTip"><?php echo JText::_('CU_INTROTEXT'); ?></label>
                            <textarea name="config[introtext]" id="config_introtext_" class="form-control" style="width: 100%; height: 150px;" aria-hidden="true">	<?php echo $this->config['introtext']; ?></textarea>
                            <br>
                            <br>
                            <label for="config[fulltext]" title="<?php echo JText::_('CU_FULLTEXT'); ?>" class="hasTip"><?php echo JText::_('CU_FULLTEXT'); ?></label>
                            <textarea name="config[fulltext]" id="config_fulltext_" class="form-control" style="width: 100%; height: 550px;" aria-hidden="true">	<?php echo $this->config['fulltext']; ?></textarea>
                    </div>
                </td>
            </tr>
            <tr>
                <td valign="top" width="322">
                    <?php
                    $panels = array(
                        array(
                            'title' => 'JGLOBAL_FIELDSET_PUBLISHING',
                            'fieldset' => 'publishing'
                        ),
                        array(
                            'title' => 'COM_CONTENT_ATTRIBS_ARTICLE_SETTINGS_LABEL',
                            'fieldset' => 'article'
                        ),
                        array(
                            'title' => 'JGLOBAL_FIELDSET_METADATA_OPTIONS',
                            'fieldset' => 'metadata'
                        )
                    ); ?>
                    <div class="whiteBox params"><?php
                        foreach ($panels as $panel) {
                            echo JText::_($panel['title']);
                            foreach ($this->form->getFieldset($panel['fieldset']) as $field) : ?>
                                <div class="control-group">
                                    <?php echo $field->label; ?>
                                    <div class="controls">
                                        <?php echo $field->input; ?>
                                    </div>
                                </div>
                            <?php endforeach;
                        } ?>
                    </div>
                </td>
            </tr>
        </table>
        <input type="hidden" name="configname" id="configname" value="<?php echo $this->config['configname']; ?>" />
        <input type="hidden" name="configid" id="configid" value="<?php echo $this->config['configid']; ?>" />
        <input type="hidden" name="option" value="com_spreadsheetdatauploader" />
        <input type="hidden" name="task" value="savesettings" />
        <input type="hidden" name="component" value="content" />
    </form>
    <div class="clr"></div>

    <div style="width: 100%;text-align: center;">
        <?php echo JText::_('CU_CONTENT_UPLOADER_FREE') . ' | Version: ' . $this->Version . ' | <a href="https://gitlab.com/Gratia-Mira/spreadsheet-data-uploader" target="_blank">Source Code</a>';?>
    </div>
</div>
